import QtQuick 2.7
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.3

Pane {
//    Column {
//        Label {
//            text: "Pixel Clock: " + cam.pixelClock + " MHz"
//        }

//        Label {
//            text: "FPS: " + cam.fps.toFixed(2) + " fps"
//        }

//        Label {
//            text: "Exposure: " + cam.exposure.toFixed(2) + " ms"
//        }

//        Item { height: 5 }

//        Label {
//            text: "Gain: " + cam.gain
//        }

//        Label {
//            text: "Red Gain: " + cam.redGain
//        }


//    }
    Material.elevation: 6

//    color: "#a0ffffff"
//    border.width: 1
//    border.color: "#303030"
//    radius: 3

//    property int padding: 5
//    height: layout.height + 2*padding
//    width: layout.width + 2*padding


    GridLayout {
        id: layout
        columns: 2
//        x: padding
//        y: padding

        Label {
            text: "Pixel Clock (MHz):"
            Layout.alignment: Qt.AlignRight
        }
//        Label {
//            text: cam.pixelClock
//        }
        TextField {
            text: cam.pixelClock
            onEditingFinished: {
                var pc = Number(text);
                if (pc < cam.pixelClockMin)
                    pc = cam.pixelClockMin;
                if (pc > cam.pixelClockMax)
                    pc = cam.pixelClockMax;
                cam.pixelClock = pc;
            }
            validator: DoubleValidator { }

        }

        Label {
            text: "FPS:"
            Layout.alignment: Qt.AlignRight
        }
//        Label {
//            text: cam.fps.toFixed(2)
//        }
        TextField {
            text: cam.fps.toFixed(4)
            onEditingFinished: cam.fps = Number(text)
            validator: DoubleValidator { }

        }

        Label {
            text: "Exposure (ms):"
            Layout.alignment: Qt.AlignRight
        }
//        Label {
//            text: cam.exposure.toFixed(2)
//        }
        TextField {
            text: cam.exposure.toFixed(4)
            onEditingFinished: cam.gain = Number(text)
            validator: DoubleValidator { }

        }

        Label {
            text: "Gain:"
            Layout.topMargin: 5
            Layout.alignment: Qt.AlignRight
        }
//        Label {
//            Layout.topMargin: 5
//            text: cam.gain
//        }
        TextField {
            text: cam.gain
            onEditingFinished: cam.gain = Number(text)
            validator: DoubleValidator { }

        }

        Label {
            text: "Red Gain:"
            Layout.alignment: Qt.AlignRight
        }
        TextField {
            text: cam.redGain
            onEditingFinished: cam.redGain = Number(text)
            validator: DoubleValidator { }

        }

//        Label {
//            text: cam.redGain
//        }


    }

//    DropShadow {
//        anchors.fill: parent
//        horizontalOffset: 3
//        verticalOffset: 3
//        radius: 8.0
//        samples: 17
//        color: "#80000000"
//        source: parent
//    }


}
