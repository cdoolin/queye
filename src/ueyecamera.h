#ifndef UEYECAMERA_H
#define UEYECAMERA_H


#include "cppimg.h"

#include <QObject>
#include <QString>
#include <QDebug>
#include <QWinEventNotifier>

#include "uEye.h"

class UeyeCamera : public QObject
{
    Q_OBJECT
//    Q_PROPERTY(QString userName READ userName WRITE setUserName NOTIFY userNameChanged)
    Q_PROPERTY(bool connected READ connected NOTIFY connectedChanged)
    Q_PROPERTY(QObject* img READ img WRITE setImg NOTIFY imgChanged)

    Q_PROPERTY(uint sizeX READ sizeX NOTIFY sizeXChanged)
    Q_PROPERTY(uint sizeY READ sizeY NOTIFY sizeYChanged)
    Q_PROPERTY(QString model READ model NOTIFY modelChanged)

    Q_PROPERTY(uint navgs READ navgs NOTIFY navgsChanged)
    Q_PROPERTY(bool capturing READ capturing NOTIFY capturingChanged)
    Q_PROPERTY(bool viewAvgs READ viewAvgs WRITE setViewAvgs NOTIFY viewAvgsChanged)

    Q_PROPERTY(qreal exposure READ exposure WRITE setExposure NOTIFY timingChanged)
    Q_PROPERTY(qreal exposureMax READ exposureMax NOTIFY timingChanged)
    Q_PROPERTY(qreal exposureMin READ exposureMin NOTIFY timingChanged)

    Q_PROPERTY(uint pixelClock READ pixelClock WRITE setPixelClock NOTIFY timingChanged)
    Q_PROPERTY(uint pixelClockMax READ pixelClockMax NOTIFY timingChanged)
    Q_PROPERTY(uint pixelClockMin READ pixelClockMin NOTIFY timingChanged)

    Q_PROPERTY(qreal fps READ fps WRITE setFps NOTIFY timingChanged)

    Q_PROPERTY(int gain READ gain WRITE setGain NOTIFY gainChanged)
    Q_PROPERTY(int redGain READ redGain WRITE setRedGain NOTIFY gainChanged)


public:
    UeyeCamera(QObject *parent = nullptr)
      : QObject(parent),
        _connected(false), _img(nullptr),
        _cam(0),
        _sizex(640), _sizey(480), _bpp(32),
        _model("No Camera"),
        _ismems(0), _ismemids(0), _nmems(0),
        _navgs(0), _dmem(0),
        _frameEvent(NULL),
        _capturing(false),
        _viewAvgs(false)
    {
        qDebug() << "new ueye camera";

        QObject::connect(&_frameNotifier, &QWinEventNotifier::activated,
                this, &UeyeCamera::onFrameEvent);

    }

    Q_INVOKABLE void connect() {
        if (!_connected)
            openCamera();
    }

    Q_INVOKABLE void disconnect() {
        if (_connected)
            closeCamera();
    }



    Q_INVOKABLE void freezeImage();
    Q_INVOKABLE void startCapture();
    Q_INVOKABLE void endCapture();

    Q_INVOKABLE void resetAvgs();
    Q_INVOKABLE void settingsAuto();
    Q_INVOKABLE void settingsMax();
    Q_INVOKABLE void saveRawFile(const QUrl url);

    // property connected
    bool connected() {
        return _connected;
    }

    //property img
    QObject* img() {
        return _img;
    }

    void setImg(QObject *img) {
        Cppimg *i = qobject_cast<Cppimg*>(img);
        if (i != _img) {
            qDebug() << "set camera img";
            _img = i;
            imgChanged();
        }
    }

    //property sizeX, sizeY
    uint sizeX() { return _sizex; }
    uint sizeY() { return _sizey; }

    // property model
    QString model() { return QString(_model); }

    // property navgs
    uint navgs() { return _navgs; }

    // property capturing
    bool capturing() { return _capturing; }

    // property viewAvgs
    bool viewAvgs() { return _viewAvgs; }
    void setViewAvgs(const bool viewavgs) {
        if (_viewAvgs != viewavgs) {
            _viewAvgs = viewavgs;
            viewAvgsChanged();
        }
    }


    // property exposure
    qreal exposure() {
        if (!_connected)
            return 0;
        double exp = 0;

        int r = is_Exposure(_cam, IS_EXPOSURE_CMD_GET_EXPOSURE, &exp, sizeof(double));
        if (r != IS_SUCCESS)
            qDebug() << "error getting exposure";
        return exp;
    }
    void setExposure(const double exp) {
        if (!_connected)
            return;

        if (exp != exposure()) {
            double expp = exp;

            int r = is_Exposure(_cam, IS_EXPOSURE_CMD_SET_EXPOSURE, &expp, sizeof(double));
            if (r != IS_SUCCESS)
                qDebug() << "error setting exposure";

            emit timingChanged();
        }
    }


    // property maxExposure, minExposure
    uint exposureMax() {
        if (!_connected)
            return 0;
        double exp = 0;

        int r = is_Exposure(_cam, IS_EXPOSURE_CMD_GET_EXPOSURE_RANGE_MAX, &exp, sizeof(double));
        if (r != IS_SUCCESS)
            qDebug() << "error getting max exposure";
        return exp;
    }
    uint exposureMin() {
        if (!_connected)
            return 0;
        double exp = 0;

        int r = is_Exposure(_cam, IS_EXPOSURE_CMD_GET_EXPOSURE_RANGE_MIN, &exp, sizeof(double));
        if (r != IS_SUCCESS)
            qDebug() << "error getting min exposure";
        return exp;
    }

    // property pixelClock
    uint pixelClock() {
        if (!_connected)
            return 0;
        uint c;
        int r = is_PixelClock(_cam, IS_PIXELCLOCK_CMD_GET, (void*)&c, sizeof(c));
        if (r != IS_SUCCESS)
            qDebug() << "error getting pixel clock";
        return c;
    }
    void setPixelClock(const uint pclock) {
        if (!_connected)
            return;

        if (pclock != pixelClock()) {
            uint c = pclock;
            int r = is_PixelClock(_cam, IS_PIXELCLOCK_CMD_SET, (void*)&c, sizeof(c));
            if (r != IS_SUCCESS)
                qDebug() << "error setting pixel clock";
            emit timingChanged();
        }
    }

    // property pixelClockMax, pixelClockMin
    uint pixelClockMax() {
        if (!_connected)
            return 0;

        uint range[3] = {0, 0, 0};
        int r = is_PixelClock(_cam, IS_PIXELCLOCK_CMD_GET_RANGE, (void*)&range, sizeof(range));
        if (r != IS_SUCCESS) {
            qDebug() << "error getting pixel clock";
            return 0;
        }
        return range[1];
    }
    uint pixelClockMin() {
        if (!_connected)
            return 0;

        uint range[3] = {0, 0, 0};
        int r = is_PixelClock(_cam, IS_PIXELCLOCK_CMD_GET_RANGE, (void*)&range, sizeof(range));
        if (r != IS_SUCCESS) {
            qDebug() << "error getting pixel clock";
            return 0;
        }
        return range[0];
    }


    // property fps
    qreal fps() {
        if (!_connected)
            return 0;

        double fps = 0;
        int r = is_GetFramesPerSecond(_cam, &fps);
        if (r != IS_SUCCESS)
            qDebug() << "unable to get fps";

        return fps;
    }

    void setFps(const double fps_) {
        if (_connected && fps_ != fps()) {
            double newfps;
            int r = is_SetFrameRate(_cam, fps_, &newfps);
            if (r != IS_SUCCESS)
                qDebug() << "error setting fps";

            emit timingChanged();
        }
    }


    // property gain
    int gain() {
        if (!_connected)
            return 0;

        return is_SetHardwareGain(_cam, IS_GET_MASTER_GAIN, IS_IGNORE_PARAMETER, IS_IGNORE_PARAMETER, IS_IGNORE_PARAMETER);
    }

    void setGain(const int gain_) {
        if (_connected && (gain() != gain_)) {
            int r = is_SetHardwareGain(_cam, gain_, IS_IGNORE_PARAMETER, IS_IGNORE_PARAMETER, IS_IGNORE_PARAMETER);
            if (r != IS_SUCCESS)
                qDebug() << "failed to set gain " << r;
            emit gainChanged();
        }
    }



    // property redGain
    int redGain() {
        if (!_connected)
            return 0;

        return is_SetHardwareGain(_cam, IS_GET_RED_GAIN, IS_IGNORE_PARAMETER, IS_IGNORE_PARAMETER, IS_IGNORE_PARAMETER);
    }

    void setRedGain(const int gain_) {
        if (_connected && (gain() != gain_)) {
            int r = is_SetHardwareGain(_cam, IS_IGNORE_PARAMETER, gain_, IS_IGNORE_PARAMETER, IS_IGNORE_PARAMETER);
            if (r != IS_SUCCESS)
                qDebug() << "failed to set gain " << r;
            emit gainChanged();
        }
    }


private:
    int openCamera();
    int closeCamera();
    void allocateImages(int num);
    void deallocateImages();
    void drawMem(uint i);
    void updateAvgs(uint i);
    void drawAvgs();
    void initEvent();
    void endEvent();

signals:
    void connectedChanged();
    void imgChanged();
    void sizeXChanged();
    void sizeYChanged();
    void modelChanged();
    void navgsChanged();
    void capturingChanged();
    void viewAvgsChanged();
    void timingChanged();
    void gainChanged();

public slots:
    void onFrameEvent();

private:
    bool _connected;

    Cppimg *_img;

    HIDS _cam;
    CAMINFO _ci;
    SENSORINFO _si;

    uint _sizex, _sizey;
    int _bpp; // bits per pixel
    QString _model;

    char **_ismems;
    int *_ismemids;
    int _nmems;

    uint _navgs;
    double *_dmem;

    HANDLE _frameEvent;
    QWinEventNotifier _frameNotifier;
    bool _capturing;
    bool _viewAvgs;
};

#endif // UEYECAMERA_H
